package de.demo.platform.cloud.fileservice.auth;

import java.util.Collections;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;

import io.dropwizard.auth.UnauthorizedHandler;

public class DefaultUnauthorizedHandler implements UnauthorizedHandler {
    @Override
    public Response buildResponse(String prefix, String realm) {
        return notAuthorized();
    }

    public static Response notAuthorized() {
        return Response.status(HttpStatus.UNAUTHORIZED_401)
                .type(MediaType.APPLICATION_JSON)
                .entity(Collections.singletonMap("message", "Not Authorized to access resource"))
                .build();
    }
}
