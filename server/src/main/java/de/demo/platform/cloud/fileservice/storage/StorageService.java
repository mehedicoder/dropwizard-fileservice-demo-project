package de.demo.platform.cloud.fileservice.storage;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.springframework.core.io.Resource;

public interface StorageService {

    void init();

    String store(FormDataContentDisposition file, InputStream fileInputStream);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

}
