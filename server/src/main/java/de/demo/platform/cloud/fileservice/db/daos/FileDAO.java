
package de.demo.platform.cloud.fileservice.db.daos;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import de.demo.platform.cloud.fileservice.api.FileMetaData;
import de.demo.platform.cloud.fileservice.util.FileMapper;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Data Access Object for objects of type {@link FileMetaData}.
 *
 */
public class FileDAO {

    /**
     * The collection of files
     */
    final MongoCollection<Document> fileCollection;

    /**
     * Constructor.
     *
     * @param fileCollection the collection of files.
     */
    public FileDAO(final MongoCollection<Document> fileCollection) {
        this.fileCollection = fileCollection;
    }

    /**
     * Find all donuts.
     *
     * @return the donuts.
     */
    public List<FileMetaData> getAll() {
        final MongoCursor<Document> files = fileCollection.find().iterator();
        final List<FileMetaData> filesFind = new ArrayList<>();
        try {
            while (files.hasNext()) {
                final Document file = files.next();
                filesFind.add(FileMapper.map(file));
            }
        } finally {
            files.close();
        }
        return filesFind;
    }

    /**
     * Get one document find in other case return null.
     *
     * @param id the identifier for find.
     * @return the FileMetaData find.
     */
    public FileMetaData getOne(final ObjectId id) {
        final Optional<Document> fileFind = Optional.ofNullable(fileCollection.find(new Document("_id", id)).first());
        return fileFind.isPresent() ? FileMapper.map(fileFind.get()) : null;
    }

    public void save(final FileMetaData fileMetaData){
        final Document saveFile =new Document("type", fileMetaData.getType())
                                      .append("path", fileMetaData.getPath());
        fileCollection.insertOne(saveFile);
    }


    /**
     * Update a register.
     *
     * @param id the identifier.
     * @param fileMetaData the object to update.
     */
    public void update(final ObjectId id, final FileMetaData fileMetaData) {
        fileCollection.updateOne(new Document("_id", id),
                new Document("$set", new Document("type", fileMetaData.getType())
                        .append("path", fileMetaData.getPath()))
        );
    }

    /**
     * Delete a register.
     * @param id the file identifier.
     */
    public void delete(final ObjectId id){
        fileCollection.deleteOne(new Document("_id", id));
    }
}
