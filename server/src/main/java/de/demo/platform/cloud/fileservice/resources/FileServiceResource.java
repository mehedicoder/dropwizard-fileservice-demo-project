
package de.demo.platform.cloud.fileservice.resources;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.demo.platform.cloud.fileservice.api.FileMetaData;
import de.demo.platform.cloud.fileservice.db.daos.FileDAO;
import de.demo.platform.cloud.fileservice.storage.FileSystemStorageService;
import de.demo.platform.cloud.fileservice.storage.StorageProperties;
import de.demo.platform.cloud.fileservice.storage.StorageService;
import org.bson.types.ObjectId;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.exb.platform.cloud.fileservice.api.FileServiceRequestUriInfo;

/**
 * This class receives all REST request and process it.
 *
 */
@Api(value = FileServiceRequestUriInfo.BASE_URI,
     description = "File Service REST API for CRUD operations on different types of file.",
     tags = {FileServiceRequestUriInfo.BASE_URI})
@Path(FileServiceRequestUriInfo.BASE_URI)
@Produces(MediaType.APPLICATION_JSON)
public class FileServiceResource {

    private StorageService storageService = new FileSystemStorageService(new StorageProperties());

    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceResource.class);

    /**
     * DAO file.
     */

    private final static String STORAGE_ROOT = "/microservice/storage";

    private FileDAO fileDAO;

    /**
     * Constructor.
     *
     * @param fileDAO the dao file.
     */
    public FileServiceResource(final FileDAO fileDAO) {
        this.fileDAO = fileDAO;
    }

    /**
     * @param inputStream
     * @param fileDisposition
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful.")
    })
    public Response save(
            @FormDataParam(value="file") final InputStream inputStream,
            @FormDataParam("file") final FormDataBodyPart body,
            @ApiParam("file") @FormDataParam(value="file") final FormDataContentDisposition fileDisposition) throws IOException {
        String storedIn = storageService.store(fileDisposition, inputStream);
        LOGGER.info(storedIn);
        LOGGER.info("Store the file into backend storage and meta data in mongo");
        FileMetaData fileMetaData = new FileMetaData();
        fileMetaData.setType(body.getMediaType().toString());
        fileMetaData.setPath(storedIn);
        fileDAO.save(fileMetaData);
        return Response.status(Response.Status.CREATED).build();
    }

    /**
     * Get all {@link FileMetaData} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful."),
            @ApiResponse(code = 404, message = "Files not found")
    })
    @GET
    public Response all() {
        LOGGER.info("List all Files.");
        final List<FileMetaData> findFiles = fileDAO.getAll();
        if (findFiles.isEmpty()) {
            return Response.accepted(new de.exb.platform.cloud.fileservice.api.Response("Files not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(findFiles).build();
    }

    /**
     * Get a {@link FileMetaData} by identifier.
     *
     * @param id the identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful."),
            @ApiResponse(code = 404, message = "Files not found")
    })
    @GET
    @Path("/{id}")
    public Response getOne(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
        LOGGER.info("Find the fileMetaData by identifier : " + id.toString());
        final FileMetaData fileMetaData = fileDAO.getOne(id);
        if (fileMetaData != null) {
            return Response.ok(fileMetaData).build();
        }
        return Response.accepted(new de.exb.platform.cloud.fileservice.api.Response("File not found.")).build();
    }



    /**
     * Update the information of a {@link FileMetaData}.
     *
     * @param id    The identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful.")
    })
    @PUT
    @Path("/{id}")
    public Response update(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id,
                           @ApiParam(value = "name") @QueryParam("name") final String newFileName) throws IOException {
        FileMetaData fileMetaData = fileDAO.getOne(id);
        String fileName = fileMetaData.getPath().substring(8);
        LOGGER.info("Update the information of a file name : {} ", fileName);
        fileMetaData.setPath("/storage/" + newFileName);
        fileDAO.update(id, fileMetaData);
        Files.move(Paths.get(STORAGE_ROOT + "/" + fileName), Paths.get(STORAGE_ROOT+ "/" + newFileName));
        return Response.ok().build();
    }

    /**
     * Delete a {@link FileMetaData} object.
      * @param id   the identifier.
     * @return  A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation successful.")
    })
    @DELETE
    @Path("/{id}")
    public Response delete(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) throws IOException {
        LOGGER.info("Delete a file from collection with identifier: " + id.toString());
        FileMetaData fileMetaData = fileDAO.getOne(id);
        fileDAO.delete(id);
        Files.deleteIfExists(Paths.get(STORAGE_ROOT + "/" + fileMetaData.getPath().substring(8)));
        return Response.ok().build();
    }

}
