
package de.demo.platform.cloud.fileservice;

import com.mongodb.BasicDBObject;
import de.demo.platform.cloud.fileservice.db.MongoDBFactoryConnection;
import de.demo.platform.cloud.fileservice.db.daos.FileDAO;
import de.demo.platform.cloud.fileservice.health.MongoDBHealthCheck;
import de.demo.platform.cloud.fileservice.resources.FileServiceResource;
import de.demo.platform.cloud.fileservice.storage.FileSystemStorageService;
import de.demo.platform.cloud.fileservice.storage.StorageProperties;
import de.demo.platform.cloud.fileservice.storage.StorageService;
import de.demo.platform.cloud.fileservice.db.MongoDBManaged;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

/**
 * This class start application.
 */
public class FileServiceApplication extends Application<FileServiceConfiguration> {

    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceApplication.class);

    /**
     * Entry point for start Application.
     *
     * @param args the args.
     * @throws Exception when the app can not start.
     */
    public static void main(final String[] args) throws Exception {
        LOGGER.info("Start application.");
        new FileServiceApplication().run(args);
        }



    @Override
    public String getName() {
        return "FileMicroservice";
    }

    @Override
    public void initialize(final Bootstrap<FileServiceConfiguration> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<FileServiceConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
                        final FileServiceConfiguration fileServiceConfiguration) {
                return fileServiceConfiguration.getSwaggerBundleConfiguration();
            }
        });
    }

    @Override
    public void run(final FileServiceConfiguration configuration,
                    final Environment environment) {

        final MongoDBFactoryConnection mongoDBManagerConn = new MongoDBFactoryConnection(configuration.getMongoDBConnection());

        final MongoDBManaged mongoDBManaged = new MongoDBManaged(mongoDBManagerConn.getClient());

        final FileDAO fileDAO = new FileDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("files"));

        BasicDBObject document = new BasicDBObject();
        mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("files").deleteMany(document);

        StorageService storageService = new FileSystemStorageService(new StorageProperties());
        storageService.deleteAll();
        storageService.init();



        environment.lifecycle().manage(mongoDBManaged);
        environment.jersey().register(MultiPartFeature.class);

        environment.jersey().register(new FileServiceResource(fileDAO));
        environment.healthChecks().register("MongoDBHealthCheck",
                new MongoDBHealthCheck(mongoDBManagerConn.getClient()));
    }

}
