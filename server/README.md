#File Service REST API

Functionality:
---
The functionality of the application is fairly simple; The application serves information about files stored in
file storage backend and allows CRUD operation on file collection. Any type of files can be stored with limited update capability. Information about file is stored in
high performance mongodb. 

URIs 
---
The endpoints for getting the user info data from the service are in the UserInfoResource class.
- GET /api/v1/files is the REST endpoint to call for a list of all the files currently stored .
- GET /api/v1/files/{id} is the REST endpoint to call for a specific file, where {id} is the unique id of the file.
- POST /api/v1/files is the REST endpoint to store the file in file storage backend.
- PUT /api/v1/files/{id} is the REST endpoint to update the meta information of the file stored in backend storage.
- FileServiceApplication contains the main method for the application and is the Application for Dropwizard to execute.

How to start the server application 
---

This microservice exposes through a REST interface REST-API that other services can use to perform file operations
 on file collection. Actual file is stored in local file storage and meta data is in mongodb.

## Technologies

- Dropwizard v. 1.3
- MongoDB Java Driver 3.8
- Swagger 1.5.2
- Mockito 2.23
- Docker

## Development

1. Run `mvn clean install` to build the server application
2. Run `docker-compose up` for start the containers
3. Create the users for database with the next commands:
    - Enter to container:
        `docker exec -it [container_name or container_id] /bin/bash`
    - Inside container call the shell for enter console of mongo db with the command 
        `mongo -uadmin -padmin`
    - Create the database
        `use files`
    - Create the user    
    - `db.createUser({ user: "user_files", pwd: "pAsw0Rd", roles: [ { role: "readWrite", db: "files"} ]});`      

Swagger
---
To check that your application is running enter url `http://localhost:8080/dropwizard-fs/swagger` 

## Authentication

To secure the File Service REST API i would suggest JSON Web Token(JWT) authentication because its a
safe way to represent a set of information between two parties. The token is composed of a header, a payload, and a signature.
JWT authentication system consists of three entities: the user, the application server, and the authentication server.
The authentication server will provide the JWT to the user. With the JWT, the user can then safely communicate with the application.

Adding below maven dependency in the server pom should be enough to start writing the implementation:


<!-- https://mvnrepository.com/artifact/io.dropwizard/dropwizard-auth -->
<dependency>
    <groupId>io.dropwizard</groupId>
    <artifactId>dropwizard-auth</artifactId>
    <version>${dropwizard.version}</version>
</dependency>

