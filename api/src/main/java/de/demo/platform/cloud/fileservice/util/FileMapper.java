/**
 * FileMetaData: FileMapper.java
 */
package de.demo.platform.cloud.fileservice.util;

import de.demo.platform.cloud.fileservice.api.FileMetaData;
import org.bson.Document;

/**
 * Mapper of FileMetaData objects and actual file in backend storage.
 *
 */
public class FileMapper {

    /**
     * Map objects {@link Document} to {@link FileMetaData}.
     *
     * @param fileDocument the information document.
     * @return A object {@link FileMetaData}.
     */
    public static FileMetaData map(final Document fileDocument) {
        final FileMetaData fileMetaData = new FileMetaData();
        fileMetaData.setId(fileDocument.getObjectId("_id"));
        fileMetaData.setType(fileDocument.getString("type"));
        fileMetaData.setPath(fileDocument.getString("path"));
        return fileMetaData;
    }
}
