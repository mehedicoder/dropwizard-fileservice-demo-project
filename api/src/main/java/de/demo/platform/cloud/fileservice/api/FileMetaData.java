
package de.demo.platform.cloud.fileservice.api;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import de.demo.platform.cloud.fileservice.util.ObjectIdSerializer;

public class FileMetaData implements Serializable {

    /** The id.*/
    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    /** The file type. */
    @NotNull
    private String type;

    /** The  path of the file in the  storage.*/
    @NotNull
    private String path;

    /**
     * Constructor.
     */
    public FileMetaData() {
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final FileMetaData fileMetaData = (FileMetaData) o;
        return Objects.equals(type, fileMetaData.type) &&
                Objects.equals(id, fileMetaData.id) &&
                Objects.equals(path, fileMetaData.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, path);
    }

    /**
     * Gets the id.
     *
     * @return the value id.
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id value.
     */
    public void setId(final ObjectId id) {
        this.id = id;
    }

    /**
     * Gets the type.
     *
     * @return the file type.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the file type.
     *
     * @param type value.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the location.
     *
     * @return the path.
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the path.
     *
     * @param path value.
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "FileMetaData{"
                + "id=" + id
                + ", type=" + type
                + ", path='" + path + '\''
                + '}';
    }
}
