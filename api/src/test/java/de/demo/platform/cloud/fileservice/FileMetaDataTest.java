package de.demo.platform.cloud.fileservice;

import de.demo.platform.cloud.fileservice.api.FileMetaData;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.testing.FixtureHelpers;

/**
 * Test class for {@link FileMetaData} model.
 */
public class FileMetaDataTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final FileMetaData fileMetaData = new FileMetaData();
        fileMetaData.setId(new ObjectId("507f1f77bcf86cd799439011"));
        fileMetaData.setType("spreadsheet");
        fileMetaData.setPath("storage/test.xlsx");

        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(FixtureHelpers.fixture("fixtures/meta.json"), FileMetaData.class));

        Assert.assertEquals(expected, MAPPER.writeValueAsString(fileMetaData));
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final FileMetaData fileMetaData = new FileMetaData();
        fileMetaData.setId(new ObjectId("507f1f77bcf86cd799439011"));
        fileMetaData.setType("spreadsheet");
        fileMetaData.setPath("storage/test.xlsx");

        Assert.assertEquals(MAPPER.readValue(FixtureHelpers.fixture("fixtures/meta.json"), FileMetaData.class), fileMetaData);
    }
}
