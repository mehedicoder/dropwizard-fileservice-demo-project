
package de.demo.platform.cloud.fileservice.client;

import de.demo.platform.cloud.fileservice.api.FileMetaData;

import java.util.Arrays;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class FileServiceClient {

    /**
     * Client to connect.
     */
    private Client client;

    /**
     * Base of URL to connect.
     */
    private String basePath;

    /**
     * Constructor.
     *
     * @param client   the client jersey.
     * @param basePath the base path.
     */
    public FileServiceClient(final Client client, final String basePath) {
        this.client = client;
        this.basePath = basePath;
    }

    /**
     * Get all {@link FileMetaData} objects.
     *
     * @return A list of {@link FileMetaData} in other case null.
     */
    public List<FileMetaData> all() {
        final WebTarget webTarget = client.target(basePath);
        final Invocation.Builder builder = webTarget.request();
        final Response response = builder.accept(MediaType.APPLICATION_JSON).get();

        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            return Arrays.asList(response.readEntity(FileMetaData[].class));
        }
        return null;
    }

    /**
     * Get a {@link FileMetaData} object.
     *
     * @param id the identifier
     * @return A object {@link FileMetaData} or null in case not found.
     */
    public FileMetaData getOne(final String id) {
        final WebTarget webTarget = client.target(basePath).path("/").path(id);
        final Invocation.Builder builder = webTarget.request();
        final Response response = builder.accept(MediaType.APPLICATION_JSON).get();

        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            return response.readEntity(FileMetaData.class);
        }
        return null;
    }

    /**
     * Persist a object of type {@link FileMetaData}.
     *
     * @param fileMetaData the fileMetaData.
     * @throws Exception when can not save.
     */
    public void save(final FileMetaData fileMetaData) throws Exception {
        final WebTarget webTarget = client.target(basePath);
        final Invocation.Builder builder = webTarget.request();
        final Response response = builder.accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(fileMetaData, MediaType.APPLICATION_JSON));

        if (Response.Status.NO_CONTENT.getStatusCode() != response.getStatus()) {
            throw new Exception("Can not save FileMetaData.");
        }
    }

    /**
     * Update the information about a {@link FileMetaData}.
     *
     * @param id    the identifier.
     * @param newFileName the new file name.
     * @throws Exception when can not update the file information.
     */
    public void update(final String id, String newFileName) throws Exception {
        final WebTarget webTarget = client.target(basePath).path("/").path(id);
        final Invocation.Builder builder = webTarget.request();
        final Response response = builder.accept(MediaType.valueOf(newFileName))
                .put(Entity.entity(newFileName, MediaType.APPLICATION_JSON));

        if (Response.Status.NO_CONTENT.getStatusCode() != response.getStatus()) {
            throw new Exception("Can not save FileMetaData.");
        }
    }

    /**
     * Delete a {@link FileMetaData} the file object.
     *
     * @param id the identifier of FileMetaData.
     */
    public void delete(final String id) throws Exception {
        final WebTarget webTarget = client.target(basePath).path("/").path(id);
        final Invocation.Builder builder = webTarget.request();
        final Response response = builder.accept(MediaType.APPLICATION_JSON)
                .delete();

        if (Response.Status.NO_CONTENT.getStatusCode() != response.getStatus()) {
            throw new Exception("Can not save FileMetaData.");
        }
    }

}
