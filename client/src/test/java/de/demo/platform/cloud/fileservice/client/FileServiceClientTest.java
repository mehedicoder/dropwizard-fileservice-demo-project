package de.demo.platform.cloud.fileservice.client;

import de.demo.platform.cloud.fileservice.api.FileMetaData;
import de.demo.platform.cloud.fileservice.api.FileServiceRequestUriInfo;
import io.dropwizard.testing.junit.DropwizardClientRule;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

/**
 * Unit test for {@link FileServiceClient}.
 */
public class FileServiceClientTest {

    private static final FileMetaData file;

    public static final String ID_FILE = "507f1f77bcf86cd799439011";

    static {
        file = new FileMetaData();
        file.setId(new ObjectId(ID_FILE));
        file.setType("application/pdf");
        file.setPath("storage/test.pdf");
    }


    @Path(FileServiceRequestUriInfo.BASE_URI)
    @Produces(MediaType.APPLICATION_JSON)
    public static class FileServiceResource {

        @GET
        public List<FileMetaData> all(){
            return Arrays.asList(file);
        }

        @GET
        @Path("/{id}")
        public FileMetaData getOne(@PathParam("id") @NotNull final String id) {
            if (id.equals(ID_FILE)) {
                final FileMetaData file = new FileMetaData();
                file.setId(new ObjectId(ID_FILE));
                file.setType("text/xml");
                file.setPath("storage/test.xml");
                return file;
            } else {
                return null;
            }
        }

        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        public void save(final FileMetaData donut) {
            if (donut == null) {
                throw new IllegalArgumentException("Information about the file is not valid.");
            }
        }

        @PUT
        @Path("/{id}")
        public void update(@PathParam("id") @NotNull final ObjectId id, @NotNull final String newFileName) {
            if (id != null) {
                //process the update.
            } else {
                throw new IllegalArgumentException("The information for update can not be null.");
            }
        }

        @DELETE
        @Path("/{id}")
        public void delete(@PathParam("id") @NotNull final String id) {
            if (id == null) {
                throw new IllegalArgumentException("Information about file is not valid.");
            }
        }
    }

    @ClassRule
    public static final DropwizardClientRule DROPWIZARD_CLIENT_RULE = new DropwizardClientRule(new FileServiceResource());

    @Test
    public void test_all(){
        final FileServiceClient donutClient = new FileServiceClient(ClientBuilder.newClient(),DROPWIZARD_CLIENT_RULE.baseUri() + FileServiceRequestUriInfo.BASE_URI);
        final List<FileMetaData> donuts = donutClient.all();
        Assert.assertNotNull(donuts);
        Assert.assertFalse(donuts.isEmpty());
    }

    @Test
    public void test_getOne() {
        final FileServiceClient fileClient = new FileServiceClient(ClientBuilder.newClient(),DROPWIZARD_CLIENT_RULE.baseUri() + FileServiceRequestUriInfo.BASE_URI);
        final FileMetaData donut = fileClient.getOne(ID_FILE);
        Assert.assertNotNull(donut);
    }

    @Test
    public void test_save() {
        final FileServiceClient fileClient = new FileServiceClient(ClientBuilder.newClient(),DROPWIZARD_CLIENT_RULE.baseUri() + FileServiceRequestUriInfo.BASE_URI);
        try {
            fileClient.save(file);
        } catch (final Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void delete() {
        final FileServiceClient fileClient = new FileServiceClient(ClientBuilder.newClient(),DROPWIZARD_CLIENT_RULE.baseUri()+ FileServiceRequestUriInfo.BASE_URI);
        try {
            fileClient.delete(ID_FILE);
        } catch (final Exception e) {

            Assert.fail();
        }

    }
}
